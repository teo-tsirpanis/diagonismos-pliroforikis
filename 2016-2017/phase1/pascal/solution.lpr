// The solution for the second stage of the 2016-2017 Panhellenic Competition of Informatics.
// Copyright © 2016 Theodore Tsirpanis. All rights reserved.
// Unauthorized use is strictly forbidden until the competition is finished.
program solution;

{$mode objfpc}{$H+}
{$IfNDef CONTEST}
{$R *.res}
{$EndIf}

uses
  Classes,
  SysUtils,
  Math;

const
  InputDataFile = 'sch.in';
  OutputDataFile = 'sch.out';
  ErrorASCIIArt =
    '__/\\\\\\\\\\\\\\\_____________________' +
    '____________________________________        ' + LineEnding +
    ' _\/\\\///////////_____________________' +
    '_____________________________________       ' + LineEnding +
    '  _\/\\\_______________________________' +
    '______________________________________      ' + LineEnding +
    '   _\/\\\\\\\\\\\______/\\/\\\\\\\___/\' +
    '\/\\\\\\\______/\\\\\_____/\\/\\\\\\\__     ' + LineEnding +
    '    _\/\\\///////______\/\\\/////\\\_\/' +
    '\\\/////\\\___/\\\///\\\__\/\\\/////\\\_    ' + LineEnding +
    '     _\/\\\_____________\/\\\___\///__\' +
    '/\\\___\///___/\\\__\//\\\_\/\\\___\///__   ' + LineEnding +
    '      _\/\\\_____________\/\\\_________' +
    '\/\\\_________\//\\\__/\\\__\/\\\_________  ' + LineEnding +
    '       _\/\\\\\\\\\\\\\\\_\/\\\________' +
    '_\/\\\__________\///\\\\\/___\/\\\_________ ' + LineEnding +
    '        _\///////////////__\///________' +
    '__\///_____________\/////_____\///__________';

  SuccessAsciiArt =
    '_____/\\\\\\\\\\\________________________________________________________' +
    '_________________________________________/\\\_________/\\\_________/\\\____        '
    +
    LineEnding + ' ___/\\\/////////\\\_____________________________________________________'
    + '________________________________________/\\\\\\\_____/\\\\\\\_____/\\\\\\\__       '
    + LineEnding + '  __\//\\\______\///_____________________________________________________'
    + '________________________________________/\\\\\\\\\___/\\\\\\\\\___/\\\\\\\\\_      '
    + LineEnding + '   ___\////\\\___________/\\\____/\\\______/\\\\\\\\______/\\\\\\\\______'
    + '/\\\\\\\\____/\\\\\\\\\\___/\\\\\\\\\\__\//\\\\\\\___\//\\\\\\\___\//\\\\\\\__     '
    + LineEnding + '    ______\////\\\_______\/\\\___\/\\\____/\\\//////_____/\\\//////_____/'
    + '\\\/////\\\__\/\\\//////___\/\\\//////____\//\\\\\_____\//\\\\\_____\//\\\\\___    '
    + LineEnding + '     _________\////\\\____\/\\\___\/\\\___/\\\___________/\\\___________/'
    + '\\\\\\\\\\\___\/\\\\\\\\\\__\/\\\\\\\\\\____\//\\\_______\//\\\_______\//\\\____   '
    + LineEnding + '      __/\\\______\//\\\___\/\\\___\/\\\__\//\\\_________\//\\\_________\'
    + '//\\///////____\////////\\\__\////////\\\_____\///_________\///_________\///_____  '
    + LineEnding + '       _\///\\\\\\\\\\\/____\//\\\\\\\\\____\///\\\\\\\\___\///\\\\\\\\__'
    + '_\//\\\\\\\\\\___/\\\\\\\\\\___/\\\\\\\\\\______/\\\_________/\\\_________/\\\____ '
    + LineEnding + '        ___\///////////_______\/////////_______\////////______\////////__'
    + '___\//////////___\//////////___\//////////______\///_________\///_________\///_____';

type
  TInts = array of word;
  TIntMap = array of DWord;
  TResults = array[0..2] of integer;
  EBypassFinalCatchLog = class(Exception);

  procedure LogError(const s: string; const args: array of const;
  const DoCrash: boolean = True);
  begin
    Writeln(StdErr, ErrorASCIIArt);
    Writeln(StdErr, Format('Error: ' + s, args));
    if DoCrash then
      raise EBypassFinalCatchLog.Create('Who lives in a pineapple under the sea?');
    //Spongebob Squarepants!
  end;

  function ParseInput: TInts;

    function GetSecondLine: string;
    var
      sl: TStringList;
    begin
      sl := TStringList.Create;
      try
        sl.LoadFromFile(InputDataFile);
        Result := sl.Strings[1];
      finally
        sl.Free;
      end;
    end;

  var
    sl: TStringList;
    TheSecondLine: string;
    i: DWord;
  begin
    TheSecondLine := GetSecondLine;
    sl := TStringList.Create;
    try
      ExtractStrings([' '], [], PChar(TheSecondLine), sl);
      SetLength(Result, sl.Count);
      if not InRange(sl.Count, 1, 100000) then
        LogError('Number count must be between 1 and 100000 (inclusive),' +
          ' but here it is %u', [sl.Count]);
      for i := 0 to sl.Count - 1 do
        Result[i] := StrToInt(sl[i]);
    finally
      sl.Free;
    end;
  end;

  {Copied from }
  function MaxIntValue(const Data: TInts): integer;
  var
    I: integer;
  begin
    Result := Data[Low(Data)];
    for I := Succ(Low(Data)) to High(Data) do
      if Data[I] > Result then
        Result := Data[I];
  end;

  function CalculateAnswer(const Integers: TInts): TResults;
  var
    TheMap: TIntMap;
    h, i: integer;
    MaxElement: integer = 0;
    MaxLocation: integer = 0;
    BiggestElement: integer = 0;
  begin
    //-----Initialization-----
    //Find the biggest element in the input array.
    //This happens in order to allocate neccessary space for the map.
    BiggestElement := MaxIntValue(Integers);
    //Set the length of the map.
    SetLength(TheMap, BiggestElement);
    //I am not sure if SetLength zeroes the array.
    //But it must be initially full of zeroes.
    //No loop is needed; just an assembler-optimized FillDWord.
    FillDWord(Pointer(TheMap)^, Length(TheMap), 0);

    //-----Map Population-----
    //The map is populated.
    //Each array element corresponds to one different client.
    //The minus one happens because arrays are zero-based.
    //It will be restored later. Don't worry!
    for i in Integers do
      TheMap[i - 1] += 1;

    //-----The Real Stuff-----
    {Now, the map is searched for its biggest item.
    I could sort the entire map, but this would not be effecient since I
    just want the top three elements.
    Assuming that n is the length of the map, a quicksort would take
    approximately O(n log n) time.
    This implementation however, takes Θ(n) time (specifically Θ(3n)).}

    {We find the max element three times in order
    to find the three biggest elements.}
    for h := 0 to High(Result) do
    begin
      {Just the "max element of an array" algorithm you would expect.
      That inner if statement might be a bottleneck due to branch prediction
      (Check also here: http://stackoverflow.com/questions/11227809).
      The Math.MaxIntValue is not appropriate, as we also need the index.}
      for i := 0 to High(TheMap) do
        if TheMap[i] > MaxElement then
        begin
          MaxLocation := i;
          MaxElement := TheMap[i];
        end;
      {The index of the highest value will be the result.}
      Result[h] := MaxLocation + 1;
      {We will reset the highest value on the map,
      so that on the next run, the program will find the
      next biggest item.}
      TheMap[MaxLocation] := 0;
      {I forgot to reset MaxElement and the program was buggy... Sorry. :-(}
      MaxElement := 0;
    end;
  end;

  {This is something like File.WriteAllText in the good and more
  community-supported .NET.
  Encoding is assumed to be ASCII. The files are just spaces and numbers!}
  procedure FileWriteString(const fn: TFilename; const s: string);
  var
    sl: TStringList;
  begin
    sl := TStringList.Create;
    try
      sl.Text := s;
      sl.SaveToFile(fn);
    finally
      sl.Free;
    end;
  end;

var
  TheNumbers: TInts;
  Result: TResults;
begin
  try
    {$IfDef DEBUG}
    if FileExists('heap.trc') then
      DeleteFile('heap.trc');
    SetHeapTraceOutput('heap.trc');
    {$EndIf DEBUG}

    if not FileExists(InputDataFile) then
      LogError('File %s does not exist.', [InputDataFile]);

    TheNumbers := ParseInput;
    Result := CalculateAnswer(TheNumbers);

    if FileExists(OutputDataFile) then
      Writeln(StdErr, Format('Warning: %s already exists; its data' +
        ' will be overwritten', [OutputDataFile]));

    FileWriteString(OutputDataFile, Format('%u %u %u',
      [Result[0], Result[1], Result[2]]));

    Writeln(StdErr, SuccessAsciiArt);
  except
    on e: Exception do
    begin
      if not (e is EBypassFinalCatchLog) then
      begin
        LogError('Unhandled exception.', [], False);
        raise;
      end
      else
        Halt(1);
    end;
  end;
  {$IfDef DEBUG}
  Writeln(StdErr);
  Write(StdErr, 'Press Enter to quit.');
  Readln;
  {$EndIf}
end.
