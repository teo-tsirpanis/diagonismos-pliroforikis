(* USER: u005
LANG: PASCAL
TASK: agora *)
program agora;

{$mode objfpc}{$H+}
{$WARN 5037 off}

uses
  SysUtils;

  // Calculates the Greatest Common Divisor.
  // Our well known Euclid's algorithm.
  // I just hope FPC detects the tail recursion.
  function gcd(x1, x2: QWord): QWord;
  begin
    if x2 = 0 then
      Result := x1
    else
      Result := gcd(x2, x1 mod x2);
  end;

  // Calculates the Least Common Multiple.
  function lcm(x1, x2: QWord): QWord;
  begin
    Result := x1 * x2 div gcd(x1, x2);
  end;

const
  // The max size of the input.
  InputSize = 1000000; // 1 million.

var
  // The actual size of the input.
  N: cardinal;
  // The input. It's a static array to make memory usage more
  // predictable and remove the overhead of dynamic memory allocations.
  x: array[1..InputSize] of cardinal;
  // LLeft[i] contains the least common multiple of the first i elements of x.
  // LRight[i] contains the least common multiple of the last N - i elements of x.
  LLeft, LRight: array[1..InputSize] of QWord;
  // Out favorite loop counter.
  i: cardinal;
  // The minimum least common multiple of all elements of the input, except (maybe) from one.
  min: QWord;
  // The position of the element that is excluded from calculating `min`.
  // If `min` is the least common multiple of _all_ elements, `minpos` is zero.
  minpos: cardinal;
  // The input and output files.
  fIn, fOut: Text;

  // Checks the LCM of x1 and x2 and, if it is smaller than min, updates minpos.
  procedure LCMCheck(x1, x2: QWord; pos: cardinal); inline;
  var
    L: QWord;
  begin
    L := lcm(x1, x2);
    if L < min then
    begin
      min := L;
      minpos := pos;
    end;
  end;

begin
  Assign(fIn, 'agora.in');
  Reset(fIn);
  Assign(fOut, 'agora.out');
  Rewrite(fOut);
  Readln(fIn, N);
  for i := 1 to N do
  begin
    Read(fIn, x[i]);
  end;

  if N = 1 then
    // First, the trivial case of only one element.
    // The description did state that N can be 1.
    // But the output could be either "0 1", or "x[1] 0".
    // Adter a discussion with fellow competition members on Facebook, I decided to do the second option.
    Writeln(fOut, '1 1')
  else
  begin

    // The algorithm is based on the fact that the Least Common Multiple has the associative property.
    // Or as functional programmers say, it is a (commutative) monoid with the identity element being 1.
    // This means that the LCM of many numbers can be efficiently calculated.

    // First, we calculate all the aggregate LCM ranges that start from the first element.
    // LLeft[1] is x[1], and LLeft[N] is the LCM of all elements.
    LLeft[1] := x[1];
    for i := 2 to N do
      LLeft[i] := lcm(LLeft[i - 1], x[i]);

    // Now we do the same, but we start from the last element and go backwards.
    // LRight[N] is x[N], and LRight[1] is the LCM of all elements.
    LRight[N] := x[N];
    for i := N - 1 downto 1 do
      LRight[i] := lcm(LRight[i + 1], x[i]);

    // We first initialize `min` and `minpos` to the LCM of all elements.
    min := LRight[1];
    minpos := 0;

    // We take the LCM of all but the first element into account.
    LCMCheck(1, LRight[2], 1);
    // And we go to the general case...
    for i := 1 to N - 2 do
      // Magic lies in the next code line...
      // The LCM of the first i elements and the last i+2 elements the LCM on all elements except from x[i]. ;-)
      LCMCheck(LLeft[i], LRight[i + 2], i + 1);
    // And the LCM of all but the last element is a special case too.
    LCMCheck(LLeft[N - 1], 1, N);

    // Now, all possible cases have been taken in the account. `in` and `inpos` contain the correct answer.
    // We write them to the output, and we are finished.

    Writeln(fOut, Format('%u %u', [min, minpos]));
  end;

  Close(fOut);
  Close(fIn);
end.
