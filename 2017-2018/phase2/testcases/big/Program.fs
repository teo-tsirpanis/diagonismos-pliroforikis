﻿// This program generates random input files for the first Greek Competition in Informatics.
// It generates only the input; not the output of course. 😛
// Made by Theodore Tsirpanis, bestowed upon the public domain (CC0)

// The RNG used is the Permuted Congruential Generator (PCG) (http://www.pcg-random.org/)

open System
open System.IO
open FsRandom

[<Literal>]
let Count = 1_000_000

[<Literal>]
let OutFile = "agora.in"

let randomNumbers = Statistics.uniformDiscrete (1, Count) |> Random.map uint32

let curry f a b = f (a, b)

[<EntryPoint>]
let main _ =
    let rng = Utility.createRandomState()
    let numbers = 
        Seq.ofRandom randomNumbers rng
        |> Seq.take Count
        |> Seq.map string
        |> String.concat " "
    sprintf "%d%s%s" Count Environment.NewLine numbers |> curry File.WriteAllText "agora.in"
    printfn "Generated an input file with %d elements" Count
    0
