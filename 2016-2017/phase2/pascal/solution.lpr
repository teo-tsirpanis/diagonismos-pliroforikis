// The solution for the second stage of the 2016-2017 Panhellenic Competition of Informatics.
// Copyright © 2017 Theodore Tsirpanis. All rights reserved.
// Unauthorized use is strictly forbidden until the competition is finished.
program solution;

{$mode objfpc}{$H+}

uses
  Classes,
  SysUtils;

const
  InputDataFile = 'uflights.in';
  OutputDataFile = 'uflights.out';

type
  TFlightTicket = record
    Cost: byte;
    Airports: array [0..1] of Dword;
  end;
  TFlightTickets = array of TFlightTicket;

  TAirport = class;
  TAirports = array of TAirport;

  { TAirport }

  TAirport = class
  strict private
    FIndex: DWord;
    FAirports: array of TAirport;
  public
    constructor Create(AIndex: DWord);
    procedure AddAirport(AIndex: DWord);
    procedure Clear;
    property Index: DWord read FIndex;
    property Airports: TAirports read FAirports;
  end;

var
  TheAirports: TAirports;
  TheFlightTickets: TFlightTickets;
  TheHolyResult: DWord;
  a: TAirport;

  {$REGION Utility Code}
  function MakeFlightTicket(ACost: byte; AAirport1, AAirport2: DWord): TFlightTicket;
  begin
    Result.Cost := ACost;
    Result.Airports[0] := AAirport1;
    Result.Airports[1] := AAirport2;
  end;

  procedure Quicksort(Left, Right: integer; AArray: TFlightTickets);
  var
    ptrLeft, ptrRight, Pivot: integer;
    Temp: TFlightTicket;
  begin
    ptrLeft := Left;
    ptrRight := Right;
    Pivot := AArray[(Left + Right) div 2].Cost;
    repeat
      while (ptrLeft < Right) and (AArray[ptrLeft].Cost < Pivot) do
        Inc(ptrLeft);
      while (ptrRight > Left) and (AArray[ptrRight].Cost > Pivot) do
        Dec(ptrRight);
      if ptrLeft <= ptrRight then
      begin
        if ptrLeft < ptrRight then
        begin
          Temp := AArray[ptrLeft];
          AArray[ptrLeft] := AArray[ptrRight];
          AArray[ptrRight] := Temp;
        end;
        Inc(ptrLeft);
        Dec(ptrRight);
      end;
    until ptrLeft > ptrRight;
    if ptrRight > Left then
      Quicksort(Left, ptrRight, AArray);
    if ptrLeft < Right then
      Quicksort(ptrLeft, Right, AArray);
  end;

  function ParseInput: TFlightTickets;
  var
    sl1, sl2: TStringList;
    s: string;
    i: DWord;
  begin
    sl1 := TStringList.Create;
    sl2 := TStringList.Create;
    try
      sl1.LoadFromFile(InputDataFile);
      ExtractStrings([' '], [], PChar(sl1[0]), sl2);
      SetLength(TheAirports, StrToInt(sl2[0]));
      for i := 0 to High(TheAirports) do
        TheAirports[i] := TAirport.Create(i);
      SetLength(Result, StrToInt(sl2[1]));
      for i := 0 to Length(Result) - 1 do
      begin
        s := sl1[i + 1];
        sl2.Clear;
        ExtractStrings([' '], [], PChar(s), sl2);
        Result[i] := MakeFlightTicket(StrToInt(sl2[2]), StrToInt(sl2[0]),
          StrToInt(sl2[1]));
      end;
    finally
      sl1.Free;
      sl2.Free;
    end;
  end;

  procedure FileWriteString(const fn: TFilename; const s: string);
  var
    sl: TStringList;
  begin
    sl := TStringList.Create;
    try
      sl.Text := s;
      sl.SaveToFile(fn);
    finally
      sl.Free;
    end;
  end;

  { TAirport }

  constructor TAirport.Create(AIndex: DWord);
  begin
    FIndex := AIndex;
  end;

  procedure TAirport.AddAirport(AIndex: DWord);
  var
    temp: TAirport;
  begin
    SetLength(FAirports, Succ(Length(FAirports)));
    temp := TheAirports[AIndex];
    Assert(temp.Index = AIndex, 'Airport index inconsistency.');
    FAirports[High(FAirports)] := temp;
  end;

  procedure TAirport.Clear;
  begin
    SetLength(FAirports, 0);
  end;

  procedure LinkAirports(const A1, A2: DWord);
  begin
    TheAirports[A1 - 1].AddAirport(A2 - 1);
    TheAirports[A2 - 1].AddAirport(A1 - 1);
  end;

  procedure ClearAirports;
  var
    a: TAirport;
  begin
    for a in TheAirports do
      a.Clear;
  end;

  {$ENDREGION}

  function NailItMan(AFlightTickets: TFlightTickets): DWord;
  var
    TicketCheckBox, AirportCheckBox, PermanentPaths: array of boolean;
    i: integer;

    function VerifyPaths: boolean;
    var
      WalkJournal: array of boolean;
      i: integer;
      b: boolean;

      procedure Walk(const AAirport: TAirport);
      var
        a: TAirport;
      begin
        if not WalkJournal[AAirport.Index] then
        begin
          WalkJournal[AAirport.Index] := True;
          for a in AAirport.Airports do
            Walk(a);
        end;
      end;

    begin
      SetLength(WalkJournal, Length(TheAirports));
      ClearAirports;
      for i := 0 to High(AFlightTickets) do
        if TicketCheckBox[i] then
          with AFlightTickets[i] do
            LinkAirports(Airports[0], Airports[1]);
      Walk(TheAirports[0]);
      Result := True;
      for b in WalkJournal do
      begin
        Result := b;
        if not Result then
          break;
      end;
      if not Result then
        for i := 0 to High(AFlightTickets) do
          with AFlightTickets[i] do
            if WalkJournal[Airports[0] - 1] xor WalkJournal[Airports[1] - 1] then
            begin
              PermanentPaths[i] := True;
              Break;
            end;
    end;

  begin
    Quicksort(0, High(AFlightTickets), AFlightTickets);
    SetLength(TicketCheckBox, Length(AFlightTickets));
    SetLength(AirportCheckBox, Length(TheAirports));
    SetLength(PermanentPaths, Length(AFlightTickets));
    repeat
      TicketCheckBox := Copy(PermanentPaths);
      FillByte(Pointer(AirportCheckBox)^, Length(AirportCheckBox), 0);
      for i := 0 to High(AFlightTickets) do
        if PermanentPaths[i] then
          with AFlightTickets[i] do
          begin
            AirportCheckBox[Airports[0]] := True;
            AirportCheckBox[Airports[1]] := True;
          end;
      for i := 0 to High(AFlightTickets) do
        with AFlightTickets[i] do
        begin
          if (not (AirportCheckBox[Airports[0] - 1] and
            AirportCheckBox[Airports[1] - 1])) then
          begin
            TicketCheckBox[i] := True;
            AirportCheckBox[Airports[0] - 1] := True;
            AirportCheckBox[Airports[1] - 1] := True;
          end;
        end;
    until VerifyPaths;
    Result := 0;
    for i := 0 to High(AFlightTickets) do
      if TicketCheckBox[i] then
        Result += AFlightTickets[i].Cost;
  end;

begin
  {$IFDEF DEBUG}
  if FileExists('heap.trc') then
    DeleteFile('heap.trc');
  SetHeapTraceOutput('heap.trc');
  {$ENDIF DEBUG}
  try
    TheFlightTickets := ParseInput;
    TheHolyResult := NailItMan(TheFlightTickets);
    FileWriteString(OutputDataFile, IntToStr(TheHolyResult));
  finally
    for a in TheAirports do
      a.Free;
  end;
end.
