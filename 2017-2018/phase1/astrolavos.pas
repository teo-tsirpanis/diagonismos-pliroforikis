(* USER: u005
LANG: PASCAL
TASK: astrolavos *)
program astrolavos;

{$mode objfpc}{$H+}

uses
  Classes,
  SysUtils;

var
  inputFile: Text;
  outputFile: Text;
  L: word;
  M: array of word;
  V: array of double;
  N: DWord;
  K1, K2, K3, K4, K5: word;
  sl: TStringList;
  i: integer;

  procedure ReadStringList;
  var
    s: string;
  begin
    Readln(inputFile, s);
    sl.DelimitedText := s;
  end;

begin
  sl := TStringList.Create;
  sl.Delimiter := ' ';
  Assign(inputFile, 'astrolavos.in');
  Reset(inputFile);
  Assign(outputFile, 'astrolavos.out');
  Rewrite(outputFile);
  Readln(inputFile, L);
  SetLength(M, L);
  SetLength(V, L);
  ReadStringList;
  for i := 0 to Pred(sl.Count) do
    M[i] := StrToInt(sl[i]);
  ReadStringList;
  N := StrToInt(sl[0]);
  K1 := StrToInt(sl[1]) - 1;
  K2 := StrToInt(sl[2]) - 1;
  K3 := StrToInt(sl[3]) - 1;
  K4 := StrToInt(sl[4]) - 1;
  K5 := StrToInt(sl[5]) - 1;
  V[0] := N;
  for i := 1 to Pred(Length(M)) do
    V[i] := V[Pred(i)] * M[Pred(i)] / M[i];
  DefaultFormatSettings.DecimalSeparator := '.';
  Writeln(outputFile, Format('%.3f %.3f %.3f %.3f %.3f',
    [V[K1], V[K2], V[K3], V[K4], V[K5]]));
  Close(inputFile);
  Close(outputFile);
  sl.Free;
end.
